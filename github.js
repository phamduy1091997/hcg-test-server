import axios from 'axios';
import queryString from 'query-string';

const getGithubLoginUrl = () => {
    const params = queryString.stringify({
        client_id: '70bf40f590aadb11904e',
        redirect_uri: 'http://localhost:3000/github/callback',
        scope: ['read:user', 'user:email'].join(' '), // space seperated string
        allow_signup: true,
    });

    return `https://github.com/login/oauth/authorize?${params}`;
}

const getAccessTokenFromCode = async (code) => {
    const { data } = await axios({
        url: 'https://github.com/login/oauth/access_token',
        method: 'get',
        params: {
            client_id: '347fba52f9e12f837255',
            client_secret: 'e66191da69240cc2c9fe5a840d2b7dab4450ad15',
            redirect_uri: 'http://localhost:4200/oauth-github',
            code,
        },
    });

    const parsedData = queryString.parse(data);
    console.log(parsedData); // { token_type, access_token, error, error_description }
    if (parsedData.error) throw new Error(parsedData.error_description)
    return parsedData.access_token;
}


export {
    getAccessTokenFromCode, getGithubLoginUrl
};
